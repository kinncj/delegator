var express    = require('express'),
    router     = express.Router(),
    md5        = require('MD5'),
    unreviewed = 0;

router.get('/', function(req, res) {
    if ( ! req.session.registered) {
        res.redirect('/register');

        return;
    }

    var reportsCollection = req.reportsCollection;

    reportsCollection.find({to: null},{},function(e,docs){
        unreviewed = docs.length;
    });

    res.render('index', {
        email: req.session.registered,
        "userData": {
            email: req.session.registered,
            emailHash: md5(req.session.registered),
            totalUnreviewed: unreviewed
        }
    });

});

router.get('/logout', function(req, res) {
    req.session.registered = false;

    res.redirect('/register');

    return;
});

router.get('/register', function(req, res) {
    res.render('register');
});

router.post('/register', function(req, res) {
    req.session.registered = req.body.email,

    res.redirect('/');
});

module.exports = router;
