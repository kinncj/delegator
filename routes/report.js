var express  = require('express'),
    router   = express.Router(),
    ObjectID = require('mongodb').ObjectID,
    md5      = require('MD5');

router.get('/', function(req, res) {
    if ( ! req.session.registered) {
        res.redirect('/register');

        return;
    }

    var reportsCollection = req.reportsCollection,
        unreviewed        = 0;

    reportsCollection.find({},{},function(e,docs){
        reportsCollection.find({to: null},{},function(e,docs2){
            unreviewed = docs2.length;
        });

        res.render('commitList', {
            "commitList" : docs,
            "userData": {
                email: req.session.registered,
                emailHash: md5(req.session.registered),
                totalUnreviewed: unreviewed
            }
        });
    });
});

router.post('/', function(req, res) {
    if ( ! req.session.registered) {
        res.redirect('/register');

        return;
    }

    var url = "http://git.dev/%type/%type/commit/%hash".replace("%type", req.body.type)
        .replace("%type", req.body.type)
        .replace("%hash", req.body.hash);


    var reportsCollection = req.reportsCollection;
    // Submit to the DB
    reportsCollection.insert({
        "_id": ObjectID(req.body.hash),
        "from" : req.session.registered,
        "commit" : req.body.hash,
        "type": req.body.type,
        "branch": req.body.branch,
        "url": url,
        "to": null
    }, function (err, doc) {
        if (err) {
            // If it failed, return error
            res.send("Commit already registered.");

            return;
        }

        // And forward to success page
        res.redirect("/report");

        return;
    });
});

module.exports = router;
