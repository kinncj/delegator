var express  = require('express'),
    router   = express.Router(),
    ObjectID = require('mongodb').ObjectID,
    md5      = require('MD5');

router.get('/:email/:commit', function(req, res) {
    if ( ! req.session.registered) {
        res.redirect('/register');

        return;
    }

    var reportsCollection = req.reportsCollection,
        commit            = req.params.commit,
        email             = req.params.email;

    reportsCollection.find({_id: commit},{},function(e,docs){


        if (docs.length == 0) {
            res.redirect('/report');

            return;
        }

        reportsCollection.update({_id: commit}, {$set:{to: req.session.registered}});

        res.redirect('/report');

        return;
    });

});

router.get('/logout', function(req, res) {
    req.session.registered = false;

    res.redirect('/register');

    return;
});

router.get('/register', function(req, res) {
    var reportsCollection = req.reportsCollection,
        unreviewed        = 0;

    reportsCollection.find({to: null},{},function(e,docs){
        unreviewed = docs.length;
    });

    res.render('register', {
        "userData": {
            email: req.session.registered,
            emailHash: md5(req.session.registered),
            totalUnreviewed: unreviewed
        }
    });
});

router.post('/register', function(req, res) {
    req.session.registered = req.body.email,

    res.redirect('/');
});

module.exports = router;
